# Sayonara

[![forthebadge](https://forthebadge.com/images/badges/contains-tasty-spaghetti-code.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com)

`Sayonara` is a customizable logout screen based on KDE Plasma's logout screen.

It's still in development, but the current version already works. I plan to add some more customization options in te near future.

Keep in mind that, as of today, `Sayonara` expects some config files to be in this directory:

`~/.config/sayonara/`

If you don't know yet how to customize it, just copy the contents of the provided `config-example/` folder to `~/.config/sayonara/`.

Contributions, testers and issues are appreciated :)

## Installation

You need to have the `rust` language installed to build and install `Sayonara`. To install `rust`, you can follow the instructions on the [official website](https://www.rust-lang.org/tools/install). Using `rustup` is by far the easiest way to install it.

You'll also need GTK's development libraries, since `Sayonara` is GTK based. You can install them, depending on your distribution, from the package manager:

- Fedora
  
  ```bash
  sudo dnf install gtk4-devel gcc
  ```

- Debian and derivatives
  
  ```bash
  sudo apt install libgtk-4-dev build-essential
  ```

- Arch and derivatives
  
  ```bash
  sudo pacman -S gtk4 base-devel
  ```

Once you have rust with all of its envioronment setup (`cargo` in particular), and you have installed GTK's libraries, you can run

```bash
cargo run --release
```

to compile `Sayonara`, and then

```bash
mkdir -p ~/.local/bin
cp target/release/sayonara ~/.local/bin/
```

for a user local installation, or

```bash
suodo cp target/release/sayonara /bin
```

for a system-wide installation.

Finally, make sure you copied the configuration example to the appropriate folder. You can use this command:

```bash
mkdir -p ~/.config/sayonara/
cp -r config-example/* ~/.config/sayonara/
```

I plan to make binarie version available in release format so you don't have to compile it every from source.