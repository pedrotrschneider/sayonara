use std::process::Command;
use std::fs;
use std::collections::HashMap;
use home;
use toml;

use gtk::prelude::*;
use gtk::{ Application, ApplicationWindow, CssProvider, StyleContext, Image, Grid, Label, Button, Align, Inhibit };
use gtk::gdk::Display;
use gtk::glib;
use gtk::gio;

const APP_ID: &str = "org.gtk_rs.HelloWorld2";

fn main() {
    let app = Application::builder().application_id(APP_ID).build();
    app.connect_startup(|_| load_css());
    app.connect_activate(build_ui);
    app.run();
}

fn load_css() {
    let home_path = match home::home_dir() {
        Some(path) => path,
        None => panic!("Could not find your home directory.\n"),
    };
    let config_path = home_path.join(".config/sayonara");
    let css_path = config_path.join("style.css");

    let provider = CssProvider::new();
    provider.load_from_file(&gio::File::for_path(css_path));

    StyleContext::add_provider_for_display(
        &Display::default().expect("Could not connect to a display."),
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );
}

fn build_ui(app: &Application) {
    // * START: config parsing
    // Config paths
    // Folders
    let home_path = match home::home_dir() {
        Some(path) => path.to_str().unwrap().to_owned(),
        None => panic!("Could not find your home directory.\n"),
    };
    let config_path = home_path.clone() + "/.config/sayonara";
    // Files
    // Scripts
    let logout_script_path = config_path.clone() + "/scripts/logout.sh";
    let shutdown_script_path = config_path.clone() + "/scripts/shutdown.sh";
    let reboot_script_path = config_path.clone() + "/scripts/reboot.sh";
    let hibernate_script_path = config_path.clone() + "/scripts/hibernate.sh";
    // Icons
    let profile_path = config_path.clone() + "/icon.png";
    let logout_icon_path = config_path.clone() + "/img/logout.png";
    let shutdown_icon_path = config_path.clone() + "/img/shutdown.png";
    let reboot_icon_path = config_path.clone() + "/img/reboot.png";
    let hibernate_icon_path = config_path.clone() + "/img/hibernate.png";
    // TOML config
    let toml_file_path = config_path.clone() + "/sayonararc.toml";
    let toml_data = fs::read_to_string(toml_file_path).unwrap();
    let config: HashMap<String, toml::Value> = toml::from_str(&toml_data).unwrap();
    let empty_map = toml::map::Map::new();
    let main_grid_config = match config.get("main_grid") {
        Some(toml::Value::Table(table)) => table,
        _ => &empty_map,
    };
    let profile_image_config = match config.get("profile_image") {
        Some(toml::Value::Table(table)) => table,
        _ => &empty_map,
    };
    let profile_grid_config = match config.get("profile_grid") {
        Some(toml::Value::Table(table)) => table,
        _ => &empty_map,
    };
    let button_images_config = match config.get("button_images") {
        Some(toml::Value::Table(table)) => table,
        _ => &empty_map,
    };
    let action_buttons_config = match config.get("action_buttons") {
        Some(toml::Value::Table(table)) => table,
        _ => &empty_map,
    };
    // * END: config parsing

    
    // * START: main grid
    // Main grid style
    let main_grid_column_spacing = match main_grid_config.get("column_spacing") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 30,
    };
    let main_grid_row_spacing = match main_grid_config.get("row_spacing") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 30,
    };
    let main_grid_column_homogenous = match main_grid_config.get("column_homogenous") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => true,
    };
    let main_grid_halign = match main_grid_config.get("halign") {
        Some(toml::Value::Integer(val)) => match val.to_owned() {
            0 => Align::Fill,
            1 => Align::Start,
            2 => Align::End,
            3 => Align::Center,
            4 => Align::Baseline,
            _ => Align::Center
        },
        _ => Align::Center,
    };
    let main_grid_valign = match main_grid_config.get("valign") {
        Some(toml::Value::Integer(val)) => match val.to_owned() {
            0 => Align::Fill,
            1 => Align::Start,
            2 => Align::End,
            3 => Align::Center,
            4 => Align::Baseline,
            _ => Align::Center
        },
        _ => Align::Center,
    };
    let main_grid_hexpand = match main_grid_config.get("hexpand") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => true,
    };
    let main_grid_vexpand = match main_grid_config.get("vexpand") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => true,
    };
    
    // Create main grid widget
    let main_grid = Grid::builder()
        .column_spacing(main_grid_column_spacing)
        .row_spacing(main_grid_row_spacing)
        .column_homogeneous(main_grid_column_homogenous)
        .halign(main_grid_halign)
        .valign(main_grid_valign)
        .hexpand(main_grid_hexpand)
        .vexpand(main_grid_vexpand)
        .build();
    // * END: main grid
    
    // * START: window
    // Create application window
    let window = ApplicationWindow::builder()
        .application(app)
        .title("My GTK App")
        .decorated(false)
        .fullscreened(true)
        .child(&main_grid)
        .build();
        
    // Add a key event handler to listen to keyboard events
    let key_event_handler = gtk::EventControllerKey::new();
    window.add_controller(&key_event_handler);
    // * END: window
    
    // * START: profile image
    // Profile image style
    let profile_image_margin_bottom = match profile_image_config.get("margin_bottom") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 20,
    };
    let profile_image_margin_top = match profile_image_config.get("margin_top") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 20,
    };
    let profile_image_margin_end = match profile_image_config.get("margin_end") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 20,
    };
    let profile_image_margin_start = match profile_image_config.get("margin_start") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 20,
    };
    let profile_image_width_request = match profile_image_config.get("width_request") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 200,
    };
    let profile_image_height_request = match profile_image_config.get("height_request") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 200,
    };
    let profile_image_vexpand = match profile_image_config.get("vexpand") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => false,
    };
    let profile_image_hexpand = match profile_image_config.get("hexpand") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => false,
    };

    // Create profile image widget
    let profile_image = Image::builder()
        .file(&profile_path)
        .margin_bottom(profile_image_margin_bottom)
        .margin_top(profile_image_margin_top)
        .margin_end(profile_image_margin_end)
        .margin_start(profile_image_margin_start)
        .width_request(profile_image_width_request)
        .height_request(profile_image_height_request)
        .vexpand(profile_image_vexpand)
        .hexpand(profile_image_hexpand)
        .build();   
    // * START: profile image

    // * START: profile label
    let profile_label_display_name = match profile_image_config.get("display_name") {
        Some(toml::Value::String(val)) => val.to_owned(),
        _ => "This is the Name I Want Sayonara to Display".to_owned(),
    };
    let profile_label = Label::builder()
        .label(&profile_label_display_name)
        .build();
    
    profile_label.add_css_class("profile-label");
    // * END: profile label

    // * START: profile grid
    // Profile grid style
    let profile_grid_column_spacing = match profile_grid_config.get("column_spacing") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 10,
    };
    let profile_grid_row_spacing = match profile_grid_config.get("row_spacing") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 10,
    };
    let profile_grid_column_homogenous = match profile_grid_config.get("column_homogenous") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => false,
    };
    let profile_grid_row_homogenous = match profile_grid_config.get("row_homogenous") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => false,
    };
    let profile_grid_halign = match profile_grid_config.get("halign") {
        Some(toml::Value::Integer(val)) => match val.to_owned() {
            0 => Align::Fill,
            1 => Align::Start,
            2 => Align::End,
            3 => Align::Center,
            4 => Align::Baseline,
            _ => Align::Center
        },
        _ => Align::Center,
    };
    let profile_grid_valign = match profile_grid_config.get("valign") {
        Some(toml::Value::Integer(val)) => match val.to_owned() {
            0 => Align::Fill,
            1 => Align::Start,
            2 => Align::End,
            3 => Align::Center,
            4 => Align::Baseline,
            _ => Align::Center
        },
        _ => Align::Center,
    };
    let profile_grid_hexpand = match profile_grid_config.get("hexpand") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => true,
    };
    let profile_grid_vexpand = match profile_grid_config.get("vexpand") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => true,
    };

    // Create profile grid widget
    let profile_grid = Grid::builder()
        .column_spacing(profile_grid_column_spacing)
        .row_spacing(profile_grid_row_spacing)
        .column_homogeneous(profile_grid_column_homogenous)
        .row_homogeneous(profile_grid_row_homogenous)
        .halign(profile_grid_halign)
        .valign(profile_grid_valign)
        .hexpand(profile_grid_hexpand)
        .vexpand(profile_grid_vexpand)
        .build();
    // * END: profile grid
    
    // * START: button images
    // Button images style
    let button_images_margin_bottom = match button_images_config.get("margin_bottom") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 20,
    };
    let button_images_margin_top = match button_images_config.get("margin_top") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 20,
    };
    let button_images_margin_end = match button_images_config.get("margin_end") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 20,
    };
    let button_images_margin_start = match button_images_config.get("margin_start") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 20,
    };

    // Create button image widgets
    let logout_image = Image::builder()
        .file(&logout_icon_path)
        .margin_bottom(button_images_margin_bottom)
        .margin_top(button_images_margin_top)
        .margin_end(button_images_margin_end)
        .margin_start(button_images_margin_start)
        .build();

    let shutdown_image = Image::builder()
        .file(&shutdown_icon_path)
        .margin_bottom(button_images_margin_bottom)
        .margin_top(button_images_margin_top)
        .margin_end(button_images_margin_end)
        .margin_start(button_images_margin_start)
        .build();

    let reboot_image = Image::builder()
        .file(&reboot_icon_path)
        .margin_bottom(button_images_margin_bottom)
        .margin_top(button_images_margin_top)
        .margin_end(button_images_margin_end)
        .margin_start(button_images_margin_start)
        .build();

    let hibernate_image = Image::builder()
        .file(&hibernate_icon_path)
        .margin_bottom(button_images_margin_bottom)
        .margin_top(button_images_margin_top)
        .margin_end(button_images_margin_end)
        .margin_start(button_images_margin_start)
        .build();
    // * END: button images

    // * START: labels
    // Create label widgets
    let logout_label = Label::builder()
        .label("Logout (L)")
        .build();
    
    let shutdown_label = Label::builder()
        .label("Shutdown (S)")
        .build();
    
    let reboot_label = Label::builder()
        .label("Reboot (R)")
        .build();
    
    let hibernate_label = Label::builder()
        .label("Hibernate (H)")
        .build();
    // * END: labels
        
    // * START: buttons
    let action_buttons_has_frame = match action_buttons_config.get("has_frame") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => false,
    };
    let action_buttons_hexpand = match action_buttons_config.get("hexpand") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => false,
    };
    let action_buttons_vexpand = match action_buttons_config.get("vexpand") {
        Some(toml::Value::Boolean(val)) => val.to_owned(),
        _ => false,
    };
    let action_buttons_width_request = match action_buttons_config.get("width_request") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 170,
    };
    let action_buttons_height_request = match action_buttons_config.get("height_request") {
        Some(toml::Value::Integer(val)) => val.to_owned() as i32,
        _ => 170,
    };

    // Create button widgets
    let logout_button = Button::builder()
        .child(&logout_image)
        .has_frame(action_buttons_has_frame)
        .hexpand(action_buttons_hexpand)
        .vexpand(action_buttons_vexpand)
        .width_request(action_buttons_width_request)
        .height_request(action_buttons_height_request)
        .build();
    
    let shutdown_button = Button::builder()
        .child(&shutdown_image)
        .has_frame(action_buttons_has_frame)
        .hexpand(action_buttons_hexpand)
        .vexpand(action_buttons_vexpand)
        .width_request(action_buttons_width_request)
        .height_request(action_buttons_height_request)
        .build();
    
    let reboot_button = Button::builder()
        .child(&reboot_image)
        .has_frame(action_buttons_has_frame)
        .hexpand(action_buttons_hexpand)
        .vexpand(action_buttons_vexpand)
        .width_request(action_buttons_width_request)
        .height_request(action_buttons_height_request)
        .build();

    let hibernate_button = Button::builder()
        .child(&hibernate_image)
        .has_frame(action_buttons_has_frame)
        .hexpand(action_buttons_hexpand)
        .vexpand(action_buttons_vexpand)
        .width_request(action_buttons_width_request)
        .height_request(action_buttons_height_request)
        .build();
    // * END: button images
    
    // * START: signals
    // Connecting signals
    let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

    logout_button.connect_clicked(
        move |_| {
            Command::new("sh")
                .arg(&logout_script_path)
                .output()
                .expect("Failed to execute logout script.");
        }
    );

    shutdown_button.connect_clicked(move |_| {
        Command::new("sh")
            .arg(&shutdown_script_path)
            .output()
            .expect("Failed to execute logout script.");
    });
    
    reboot_button.connect_clicked(move |_| {
        Command::new("sh")
            .arg(&reboot_script_path)
            .output()
            .expect("Failed to execute logout script.");
    });
    
    hibernate_button.connect_clicked(move |_| {
        Command::new("sh")
            .arg(&hibernate_script_path)
            .output()
            .expect("Failed to execute logout script.");
    });

    
    key_event_handler.connect_key_pressed(glib::clone!(@weak logout_button, @weak shutdown_button, @weak reboot_button, @weak hibernate_button => @default-return Inhibit(false),
        move |_, key, _, _| {
            let key_name = key.name().unwrap().to_string();
            if key_name.eq("Escape") {
                let sender = sender.clone();
                std::thread::spawn(move || {
                    sender.send(false).expect("Could not send message through channel");
                    std::thread::sleep(std::time::Duration::from_millis(1600));
                    sender.send(true).expect("Could not send message through channel");
                });
            } else if key_name.eq("l") {
                logout_button.emit_clicked();
            } else if key_name.eq("s") {
                shutdown_button.emit_clicked();
            } else if key_name.eq("r") {
                reboot_button.emit_clicked();
            } else if key_name.eq("h") {
                hibernate_button.emit_clicked();
            }
            Inhibit(false)
        }
    ));

    receiver.attach(
        None,
        glib::clone!(@weak window => @default-return Continue(false),
                    move |destroy_window| {
                        window.add_css_class("close");
                        window.remove_css_class("open");
                        if destroy_window {
                            window.destroy();
                        }
                        Continue(true)
                    }
        ),
    );
    // * END: signals

    // Building UI tree
    profile_grid.attach(&profile_image, 0, 0, 1, 1);
    profile_grid.attach(&profile_label, 0, 1, 1, 1);
    main_grid.attach(&profile_grid, 0, 0, 4, 1);
    // Adding buttons to main grid
    main_grid.attach(&hibernate_button, 0, 1, 1, 1);
    main_grid.attach(&reboot_button, 1, 1, 1, 1);
    main_grid.attach(&shutdown_button, 2, 1, 1, 1);
    main_grid.attach(&logout_button, 3, 1, 1, 1);
    // Adding labels to main grid
    main_grid.attach(&hibernate_label, 0, 2, 1, 1);
    main_grid.attach(&reboot_label, 1, 2, 1, 1);
    main_grid.attach(&shutdown_label, 2, 2, 1, 1);
    main_grid.attach(&logout_label, 3, 2, 1, 1);

    window.add_css_class("open");
    window.present();
}